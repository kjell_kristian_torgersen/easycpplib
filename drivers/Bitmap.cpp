/*
 * Bitmap.cpp
 *
 *  Created on: Jun 8, 2017
 *      Author: kjell
 */

#include <Bitmap.hpp>

Bitmap::Bitmap()
{
	// TODO Auto-generated constructor stub

}

Bitmap::Bitmap( Rect size) : size(size)
{
	pixels.resize(size.Width*size.Height, {0.0,0.0,0.0,1.0});
}

const  Rect& Bitmap::Size() const
{
	return size;
}

Pixel& Bitmap::operator ()(unsigned int x, unsigned int y)
{
	return pixels.at(x+y*size.Width);
}

const Pixel& Bitmap::operator ()(unsigned int x, unsigned int y) const
{
	return pixels.at(x+y*size.Width);
}

Bitmap::~Bitmap()
{

}

