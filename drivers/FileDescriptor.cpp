/*
 * DataSource.cpp
 *
 *  Created on: Jan 28, 2016
 *      Author: kjell
 */
#include <cstddef>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <gc_cpp.h>
#include "FileDescriptor.hpp"
#include <sstream>
#include "global.hpp"

int writeall(int s, const void *buf, ssize_t len)
{
	unsigned int total = 0;        // how many bytes we've sent
	ssize_t bytesleft = len; // how many we have left to send
	int n;

	while (total < len) {
		n = (int) write(s, (char*) buf + total, (ssize_t) bytesleft);
		if (n == -1) {
			break;
		}
		total += n;
		bytesleft -= n;
	}

	//len = total; // return number actually sent here

	return n == -1 ? -1 : 0; // return -1 on failure, 0 on success
}

FileDescriptor::FileDescriptor()
{
	this->_fd = -1;
}

FileDescriptor::~FileDescriptor()
{
}

FileDescriptor::FileDescriptor(int fd)
{
	this->_fd = fd;
}

int FileDescriptor::GetFd() const
{
	return _fd;
}

int FileDescriptor::Close()
{
	int ret = close(_fd);
	_fd = -1;
	return ret;
}

bool FileDescriptor::IsOpen()
{
	if (fcntl(_fd, F_GETFL) < 0 && errno == EBADF) {
		// file descriptor is invalid or closed
		return false;
	}
	return true;
}

void FileDescriptor::Write(ByteArray data)
{
	writeall(_fd, &data[0], data.size());
}

void FileDescriptor::Write(void* data, size_t n)
{
	writeall(_fd, data, n);
}

char FileDescriptor::ReadByte()
{
	char c;
	ssize_t result = read(_fd, &c, 1);
	(void) result;
	return c;
}

size_t FileDescriptor::Read(uint8_t * buf, size_t maxBytes)
{
	return read(_fd, (void*) buf, maxBytes);
}

ByteArray FileDescriptor::Read(size_t maxBytes)
{
	std::vector<uint8_t> block;
	size_t bytesRead;

	block.resize(maxBytes);
	bytesRead = read(_fd, &block[0], maxBytes);
	ByteArray array((int)bytesRead);
	for(size_t i = 0; i < bytesRead; i++) array[(int)i] = block[i];

	return array;
}

void FileDescriptor::Write(std::string str)
{
	writeall(_fd, str.c_str(), str.length());
}

std::string FileDescriptor::ToString()
{
	std::stringstream ss;
	ss << FileDescriptor::GetType() << ": " << "fd = " << _fd << (IsOpen() ? " (open). " : "(closed). ") << Object::ToString();
	return ss.str();
}

std::string FileDescriptor::ReadLine(void)
{
	std::string s = "";
	char c;
	while ((c = ReadByte()) != '\n') {
		if(c != '\r') s += c;
	}
	return s;
}

std::string FileDescriptor::GetType()
{
	return "DataSource";
}
