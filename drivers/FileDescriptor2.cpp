/*
 * FileDescriptor2.cpp
 *
 *  Created on: Jun 11, 2017
 *      Author: kjell
 */

#include <unistd.h>
#include <drivers/FileDescriptor2.hpp>
#include <vector>

FileDescriptor2::FileDescriptor2()
{
	// TODO Auto-generated constructor stub

}

FileDescriptor2::FileDescriptor2(int fd) :
								fd(fd)
{
}

int FileDescriptor2::GetFd() const
{
	return fd;
}

void FileDescriptor2::Write(std::vector<char> arr)
{
	WriteAll(arr.data(), arr.size());
}

void FileDescriptor2::Write(char* buf, int n)
{
	WriteAll(buf, n);
}

std::vector<char> FileDescriptor2::Read()
{
}

int FileDescriptor2::Read(char* buf, int maxBytes)
{
}

bool FileDescriptor2::IsOpen()
{
}

int FileDescriptor2::Close()
{
}

void FileDescriptor2::WriteAll(char* buf, int len)
{
	unsigned int total = 0;        // how many bytes we've sent
	ssize_t bytesleft = len; // how many we have left to send
	int n;

	while (total < len) {
		n = (int) write(fd, (char*) buf + total, (ssize_t) bytesleft);
		if (n == -1) {
			break;
		}
		total += n;
		bytesleft -= n;
	}

	//len = total; // return number actually sent here

	//return n == -1 ? -1 : 0; // return -1 on failure, 0 on success
}

FileDescriptor2::~FileDescriptor2()
{
	// TODO Auto-generated destructor stub
}

