/*
 * FileDescriptor2.hpp
 *
 *  Created on: Jun 11, 2017
 *      Author: kjell
 */

#ifndef FILEDESCRIPTOR2_HPP_
#define FILEDESCRIPTOR2_HPP_

#include <interfaces/IFileDescriptor2.hpp>

class FileDescriptor2: public IFileDescriptor2
{
private:
	int fd;
	void WriteAll(char * data, int n);
public:
	FileDescriptor2();
	FileDescriptor2(int fd);
	int GetFd() const;
	void Write(std::vector<char> arr);
	void Write(char * buf, int n);
	std::vector<char> Read();
	int Read(char * buf, int maxBytes);
	bool IsOpen();
	int Close();
	virtual ~FileDescriptor2();
};

#endif /* FILEDESCRIPTOR2_HPP_ */
