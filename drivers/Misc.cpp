/*
 * Misc.cpp
 *
 *  Created on: Feb 6, 2016
 *      Author: kjell
 */

#include <sstream>
#include <fstream>
#include <vector>
#include <string>

#include <termios.h>
#include <unistd.h>
#include <memory>

#include "global.hpp"
#include "FileDescriptor.hpp"
#include "SerialPort.hpp"
#include "interfaces/IImage.hpp"
#include "Bitmap.hpp"

namespace misc {
using namespace std;

void ReadPPM(const std::string& filename, Bitmap& image)
{
	double max_col_val;
	unsigned int width;
	unsigned int height;

	std::ifstream inp(filename, std::ios::in | std::ios::binary);
	if (inp.is_open()) {
		std::string line;
		std::getline(inp, line);
		if (line != "P6") {
			std::cerr << "Error. Unrecognized file format." << std::endl;
			return ;
		}
		std::getline(inp, line);
		while (line[0] == '#') {
			std::getline(inp, line);
		}
		std::stringstream dimensions(line);

		try {
			dimensions >> width;
			dimensions >> height;
		} catch (std::exception &e) {
			std::cerr << "Header file format error. " << e.what() << std::endl;
			return ;
		}

		std::getline(inp, line);
		std::stringstream max_val(line);
		try {
			max_val >> max_col_val;
		} catch (std::exception &e) {
			std::cerr << "Header file format error. " << e.what() << std::endl;
			return ;
		}
		image = Bitmap( { width, height });

		unsigned char aux;
		double r, g, b;
		for (unsigned int y = 0; y < height; ++y) {
			for (unsigned int x = 0; x < width; ++x) {
				inp.read((char*) &aux, 1);
				r = (double) aux;
				inp.read((char*) &aux, 1);
				g = (double) aux;
				inp.read((char*) &aux, 1);
				b = (double) aux;
				image(x,y) = {r/max_col_val,g/max_col_val,b/max_col_val,1.0};
			}
		}
	} else {
		std::cerr << "Error. Unable to open " << filename << std::endl;
	}
	inp.close();

	return ;
}
double sat(double d) {
	return d > 255.0 ? 255.0 : d;
}
void WritePPM(const std::string& filename, const IImage& image)
{
	std::ofstream inp(filename.c_str(), std::ios::out | std::ios::binary);
	if (inp.is_open()) {
		inp << "P6\n";
		inp << image.Size().Width;
		inp << " ";
		inp << image.Size().Height << "\n";
		inp << 255 << "\n";
		unsigned char aux;
		//for (auto& p : pixels) {
		for (unsigned int y = 0; y < image.Size().Height; y++) {
			for (unsigned int x = 0; x < image.Size().Width; x++) {
				auto& p = image(x,y);
				aux = (unsigned char) sat(255.0 * p.R);
				inp.write((char*) &aux, 1);
				aux = (unsigned char) sat(255.0 * p.G);
				inp.write((char*) &aux, 1);
				aux = (unsigned char) sat(255.0 * p.B);
				inp.write((char*) &aux, 1);
			}
		}
	} else {
		std::cout << "Error. Unable to open " << filename << std::endl;
	}
	inp.close();
}

void WriteAllBytes(string path, const char * bytes, size_t n)
{
	ofstream outfile(path, ios::out | ios::binary);
	outfile.write(bytes, n);
	outfile.close();
}

std::vector<char> ReadAllBytes(string path)
{
	ifstream ifs(path, ios::binary | ios::ate);
	ifstream::pos_type pos = ifs.tellg();

	std::vector<char> result(pos);

	ifs.seekg(0, ios::beg);
	ifs.read(&result[0], pos);

	return result;
}

// You could also take an existing vector as a parameter.
vector<string> split(string str, char delimiter)
{
	vector<string> internal;
	stringstream ss(str); // Turn the string into a stream.
	string tok;

	while (getline(ss, tok, delimiter)) {
		internal.push_back(tok);
	}

	return internal;
}

IFileDescriptor * ConnectSerial(std::string port, tcflag_t baud)
{
	return new SerialPort(port, baud);
}

IFileDescriptor * ConnectFd(int fd)
{
	return new FileDescriptor(fd);
}

}
