/*
 * PPMImage.cpp
 *
 *  Created on: Jun 8, 2017
 *      Author: kjell
 */
#include <assert.h>

#include <iostream>
#include <fstream>
#include <sstream>

#include "PPMImage.hpp"

PPMImage::PPMImage(Rect size = { 0, 0 }) :
								size(size)
{
	pixels.resize(this->size.Width * this->size.Height, { 0, 0, 0, 0 });
}

PPMImage::PPMImage(const std::string& filename)
{
	Load(filename);
}

const Rect& PPMImage::Size() const
{
	return size;
}

Pixel& PPMImage::operator ()(unsigned int x, unsigned int y)
{
	assert(x < size.Width && y < size.Height);
	return pixels.at(y * size.Width + x);
}

void PPMImage::Save(const std::string& filename)
{
	std::ofstream inp(filename.c_str(), std::ios::out | std::ios::binary);
	if (inp.is_open()) {

		inp << "P6\n";
		inp << size.Width;
		inp << " ";
		inp << size.Height << "\n";
		inp << 255 << "\n";
		unsigned char aux;
		for (auto& p : pixels) {
			aux = (unsigned char) (255.0*p.R);
			inp.write((char*) &aux, 1);
			aux = (unsigned char) (255.0*p.G);
			inp.write((char*) &aux, 1);
			aux = (unsigned char) (255.0*p.B);
			inp.write((char*) &aux, 1);
		}
	} else {
		std::cout << "Error. Unable to open " << filename << std::endl;
	}
	inp.close();
}

void PPMImage::Load(const std::string& filename)
{
	double max_col_val;
	std::ifstream inp(filename.c_str(), std::ios::in | std::ios::binary);
	if (inp.is_open()) {
		std::string line;
		std::getline(inp, line);
		if (line != "P6") {
			std::cerr << "Error. Unrecognized file format." << std::endl;
			return;
		}
		std::getline(inp, line);
		while (line[0] == '#') {
			std::getline(inp, line);
		}
		std::stringstream dimensions(line);

		try {
			dimensions >> size.Width;
			dimensions >> size.Height;
		} catch (std::exception &e) {
			std::cerr << "Header file format error. " << e.what() << std::endl;
			return;
		}

		std::getline(inp, line);
		std::stringstream max_val(line);
		try {
			max_val >> max_col_val;
		} catch (std::exception &e) {
			std::cerr << "Header file format error. " << e.what() << std::endl;
			return;
		}
		pixels.clear();
		pixels.reserve(this->size.Width * this->size.Height);

		unsigned char aux;
		double r, g, b, a = 1.0;
		for (unsigned int i = 0; i < size.Width * size.Height; ++i) {
			inp.read((char*) &aux, 1);
			r = (double) aux;
			inp.read((char*) &aux, 1);
			g = (double) aux;
			inp.read((char*) &aux, 1);
			b = (double) aux;
			pixels.push_back( { r / max_col_val, g / max_col_val, b / max_col_val, a });
		}
	} else {
		std::cerr << "Error. Unable to open " << filename << std::endl;
	}
	inp.close();
}

PPMImage::~PPMImage()
{
	// TODO Auto-generated destructor stub
}

