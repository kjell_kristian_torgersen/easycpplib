/*
 * PackageHandler.cpp
 *
 *  Created on: Jan 30, 2016
 *      Author: kjell
 */

#include "PackageHandler.hpp"
#include <cstring>
#include <typeinfo>
#include "global.hpp"
#include "SafeArray.hpp"
#include <sstream>
#include "Object.hpp"

PackageHandler::PackageHandler(PackageReceivedCallback callBack)
{
	InterpretPackage = callBack;
	PackageIndex = 0;
	PackageChecksum = 0;
}

void PackageHandler::ReceivePackage(ByteArray& package)
{
	unsigned char b;
	int i;

	for (i = 0; i < package.size(); i++) {
		b = package[i];
		switch (PackageIndex) {
		case 0:
			if (b == START_BYTE) {
				PackageIndex = 1;
				PackageChecksum = b;
			}
			break;
		case 1:
			PackageChecksum += b;
			PackageIndex = 2;
			Package = ByteArray(b);
			break;
		default:
			if (PackageIndex - 2 < Package.size()) {
				Package[PackageIndex - 2] = (uint8_t) b;
			}
			PackageChecksum += b;
			PackageIndex++;
			if (PackageIndex >= Package.size() + 3) {
				PackageIndex = 0;
				while (PackageChecksum >= 0x100) {
					PackageChecksum = (PackageChecksum >> 8) + (PackageChecksum & 0xFF);
				}
				PackageChecksum = (~PackageChecksum) & 0xFF;
				if (PackageChecksum == 0) {
					InterpretPackage(Package);
				} else {
				}
			}
			break;
		}
	}
}

ByteArray PackageHandler::CreatePackage(ByteArray& payload)
{
	int i;
	unsigned int checksum = 0;

	if (payload.size() > 255) {
		throw std::out_of_range("Cannot create packages greater than 255 bytes!");
	}
	ByteArray package(payload.size() + 3);
	package[0] = (uint8_t) START_BYTE;
	package[1] = (uint8_t) (payload.size());
	memcpy(&package[2], &payload[0], payload.size());
	for (i = 0; i < package.size(); i++) {
		checksum += package[i];
		while (checksum > 255)
			checksum -= 255;
	}
	package[package.size() - 1] = (unsigned char) (~checksum);
	return package;
}

PackageHandler::~PackageHandler()
{

}

std::string PackageHandler::ToString()
{
	std::stringstream ss;
	std::string package = "NULL";
	ss << typeid(PackageHandler).name() << ": InterpretPackage = " << &InterpretPackage << ", Package=(" << package << "), PackageIndex = " << PackageIndex
									<< ", PackageChecksum = " << PackageChecksum << ". " << Object::ToString();
	return ss.str();
}
