/*
 * TCPClient.cpp
 *
 *  Created on: Jun 7, 2017
 *      Author: kjell
 */

#include "TCPClient.hpp"
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string>
#include <string.h>
#include <memory.h>
#include <arpa/inet.h>

TCPClient::TCPClient()
{
	// TODO Auto-generated constructor stub

}

TCPClient::TCPClient(std::string hostname, int port)
{
}
#define MAXBUFLEN 256
// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int TCPClient::Connect(std::string hostname, int port)
{
	    struct addrinfo hints, *servinfo, *p;
	    int rv;
	    int numbytes;
	    struct sockaddr_storage their_addr;
	    char buf[MAXBUFLEN];
	    std::string prt = std::to_string(port);
	    socklen_t addr_len;
	    char s[INET6_ADDRSTRLEN];

	    memset(&hints, 0, sizeof hints);
	    hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
	    hints.ai_socktype = SOCK_DGRAM;
	    hints.ai_flags = AI_PASSIVE; // use my IP

	    if ((rv = getaddrinfo(NULL, prt.c_str(), &hints, &servinfo)) != 0) {
	        std::cerr << "getaddrinfo: " << gai_strerror(rv) << std::endl;
	        return 1;
	    }

	    // loop through all the results and bind to the first we can
	    for(p = servinfo; p != NULL; p = p->ai_next) {
	        if ((_fd = socket(p->ai_family, p->ai_socktype,
	                p->ai_protocol)) == -1) {
	            perror("listener: socket");
	            continue;
	        }

	        if (bind(_fd, p->ai_addr, p->ai_addrlen) == -1) {
	            close(_fd);
	            perror("listener: bind");
	            continue;
	        }

	        break;
	    }

	    if (p == NULL) {
	        std::cerr << "listener: failed to bind socket" << std::endl;
	        return 2;
	    }

	    freeaddrinfo(servinfo);

	    std::cout << "listener: waiting to recvfrom...\n";

	  /*  addr_len = sizeof their_addr;
	    if ((numbytes = recvfrom(_fd, buf, MAXBUFLEN-1 , 0,
	        (struct sockaddr *)&their_addr, &addr_len)) == -1) {
	        perror("recvfrom");
	        exit(1);
	    }

	    printf("listener: got packet from %s\n",
	        inet_ntop(their_addr.ss_family,
	            get_in_addr((struct sockaddr *)&their_addr),
	            s, sizeof s));
	    printf("listener: packet is %d bytes long\n", numbytes);
	    buf[numbytes] = '\0';
	    printf("listener: packet contains \"%s\"\n", buf);

	    close(_fd);*/
	    return 0;
}

TCPClient::~TCPClient()
{
	// TODO Auto-generated destructor stub
}

