/*
 * TCPClient.hpp
 *
 *  Created on: Jun 7, 2017
 *      Author: kjell
 */

#ifndef TCPCLIENT_HPP_
#define TCPCLIENT_HPP_

#include <FileDescriptor.hpp>


class TCPClient: public FileDescriptor
{
private:
	std::string hostname;
	int port;
public:
	TCPClient();
	TCPClient(std::string hostname, int port);
	int Connect(std::string hostname, int port);
	virtual ~TCPClient();
};

#endif /* TCPCLIENT_HPP_ */
