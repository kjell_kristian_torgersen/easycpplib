/*
 * Bitmap.hpp
 *
 *  Created on: Jun 8, 2017
 *      Author: kjell
 */

#ifndef BITMAP_HPP_
#define BITMAP_HPP_

#include <vector>
#include <interfaces/IImage.hpp>

class Bitmap: public IImage
{
	std::vector<Pixel> pixels;
	Rect size;
public:
	Bitmap();
	Bitmap(Rect size);
	const Rect & Size() const;
	Pixel & operator()(unsigned int x, unsigned int y);
	const Pixel & operator()(unsigned int x, unsigned int y)const;
	virtual ~Bitmap();
};

#endif /* BITMAP_HPP_ */
