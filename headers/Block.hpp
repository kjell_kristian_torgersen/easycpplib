/*
 * Block.hpp
 *
 *  Created on: Jan 28, 2016
 *      Author: kjell
 */

#ifndef BLOCK_HPP_
#define BLOCK_HPP_

#include <stdexcept>      // std::out_of_range
#include "global.hpp"

class Block
{
public:
	size_t Size; /**< Size of data block in bytes */
	unsigned char * Data; /**< The data in itself */
	Block(size_t size);
	Block(unsigned char *data);
	Block(unsigned char *data, size_t size);
	~Block();
};

#endif /* BLOCK_HPP_ */

