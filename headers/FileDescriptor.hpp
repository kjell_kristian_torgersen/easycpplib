/*
 * DataSource.hpp
 *
 *  Created on: Jan 28, 2016
 *      Author: kjell
 */

#ifndef DATASOURCE_HPP_
#define DATASOURCE_HPP_

#include <iostream>
#include <cstddef>
#include "SafeArray.hpp"
#include <vector>
#include "global.hpp"
#include "Object.hpp"
class IFileDescriptor: public Object
{
public:
	virtual int Close()=0;
	virtual bool IsOpen()=0;
	virtual void Write(ByteArray array)=0;
	virtual void Write(std::string str)=0;
	virtual void Write(void * data, size_t n)=0;
	virtual size_t Read(unsigned char * buf, size_t maxBytes)=0;
	virtual std::string ReadLine(void)=0;
	virtual ByteArray Read(size_t maxBytes)=0;
	virtual char ReadByte()=0;
	virtual int GetFd() const=0;
	virtual std::string ToString()=0;
	virtual std::string GetType()=0;
};



class FileDescriptor: public IFileDescriptor
{
protected:
	int _fd;
public:
	FileDescriptor();
	FileDescriptor(int fd);
	~FileDescriptor();
	int Close();
	bool IsOpen();
	void Write(ByteArray array);
	void Write(std::string str);
	void Write(void * data, size_t n);
	size_t Read(unsigned char * buf, size_t maxBytes);
	std::string ReadLine(void);
	ByteArray Read(size_t maxBytes);
	char ReadByte();
	int GetFd() const;
	std::string ToString();
	std::string GetType();
};

#endif /* DATASOURCE_HPP_ */
