/*
 * Misc.hpp
 *
 *  Created on: Jun 8, 2017
 *      Author: kjell
 */

#ifndef MISC_HPP_
#define MISC_HPP_

#include "interfaces/IImage.hpp"
#include <string>

namespace misc {
void ReadPPM(const std::string& filename, Bitmap& image);
void WritePPM(const std::string& filename, const IImage& image);
}

#endif /* MISC_HPP_ */
