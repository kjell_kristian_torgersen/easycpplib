/*
 * Object.hpp
 *
 *  Created on: Feb 1, 2016
 *      Author: kjell
 */

#ifndef OBJECT_HPP_
#define OBJECT_HPP_

#include <iostream>
#include "global.hpp"

class IObject
{
public:
	 virtual std::string ToString()=0;
	 virtual ~IObject(){};
};

class Object : public IObject
{
public:
	 std::string ToString();
	 ~Object();
};

class Other : public Object
{
public:
	 //std::string ToString();
};

#endif /* OBJECT_HPP_ */
