/*
 * PPMImage.hpp
 *
 *  Created on: Jun 8, 2017
 *      Author: kjell
 */

#ifndef PPMIMAGE_HPP_
#define PPMIMAGE_HPP_

#include <vector>
#include <string>
#include "interfaces/IImage.hpp"

class PPMImage: public IImage
{
	Rect size;
	std::vector<Pixel> pixels;
public:
	PPMImage(Rect size);
	PPMImage(const std::string& filename);
	void Save(const std::string& filename);
	void Load(const std::string& filename);
	const Rect & Size()const;
	Pixel & operator()(unsigned int x, unsigned int y);
	virtual ~PPMImage();
};

#endif /* PPMIMAGE_HPP_ */
