/*
 * Publisher.hpp
 *
 *  Created on: Sep 10, 2016
 *      Author: kjell
 */

#ifndef PUBLISHER_HPP_
#define PUBLISHER_HPP_

#include <algorithm>
#include <vector>


template<typename ... Args>
class Subscriber
{
public:
	virtual void Update(Args...)=0;
	virtual ~Subscriber() { }
};

template<class... Args>
class Publisher
{
public:
	void Publish(Args... args)
	{
		for (auto sub : subscribers) {
			sub->Update(args...);
		}
	}
	Publisher<Args...>& operator+=(Subscriber<Args...>* sub)
	{
		subscribers.push_back(sub);
		return *this;
	}

	Publisher<Args...>& operator-=(Subscriber<Args...>* sub)
	{
		auto it = std::find(subscribers.begin(), subscribers.end(), sub);
		subscribers.erase(it);
		return *this;
	}
private:
	std::vector<Subscriber<Args...>*> subscribers;
};

#endif /* PUBLISHER_HPP_ */
