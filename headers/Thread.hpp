/*
 * Thread.hpp
 *
 *  Created on: Jan 31, 2016
 *      Author: kjell
 */

#ifndef THREAD_HPP_
#define THREAD_HPP_

#include <pthread.h>
#include "global.hpp"

typedef void* (*ThreadCallBack)(void *);

class Thread
{
private:
	pthread_t threadid;
public:
	static void Init();
	Thread(ThreadCallBack callBack, void *argument);
	Thread(ThreadCallBack callBack);
	static void Exit(void * arg);
	~Thread();
};

#endif /* THREAD_HPP_ */
