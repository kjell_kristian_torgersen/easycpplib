#ifndef __IFILEDESCRIPTOR2_H__
#define __IFILEDESCRIPTOR2_H__
#include <vector>

class IFileDescriptor2
{
public:
	virtual int GetFd() const=0;
	virtual void Write(std::vector<char> arr)=0;
	virtual void Write(char * buf, int n)=0;
	virtual std::vector<char> Read()=0;
	virtual int Read(char * buf, int maxBytes)=0;
	virtual bool IsOpen()=0;
	virtual int Close()=0;
	virtual ~IFileDescriptor2() { }
};
#endif
