/*
 * IImage.hpp
 *
 *  Created on: Jun 8, 2017
 *      Author: kjell
 */

#ifndef IIMAGE_HPP_
#define IIMAGE_HPP_
struct Pixel {
	double R;
	double G;
	double B;
	double A;
};
struct Rect {
	unsigned int Width;
	unsigned int Height;
};

class IImage
{
public:
	virtual const struct Rect & Size()const=0;
	virtual Pixel & operator()(unsigned int x, unsigned int y)=0;
	virtual const Pixel & operator()(unsigned int x, unsigned int y)const=0;
	virtual ~IImage(){}
};

#endif /* IIMAGE_HPP_ */
