//============================================================================
// Name        : easycpplib.cpp
// Author      : Kjell Kristian Torgersen
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <cstdio>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>
#include <signal.h>
#include <vector>
#include <typeinfo>
#include <iomanip>

//#include <gc/gc_cpp.h>

#include "SerialPort.hpp"
#include "Multiplexer.hpp"
#include "FileDescriptor.hpp"
#include "FixedArray.hpp"
#include "Timer.hpp"
#include "PackageHandler.hpp"
#include "Thread.hpp"
#include "Object.hpp"
#include "global.hpp"
#include "Matrix.hpp"
#include <SafeArray.hpp>
#include "Publisher.hpp"
#include "Bitmap.hpp"
#include "interfaces/IImage.hpp"
#include "Misc.hpp"

using namespace std;

SerialPort * sp = NULL;
//PackageHandler * ph = NULL;

typedef enum PackageTag
{
	PT_GET = 0, PT_SET = 1, PT_DATA = 2, PT_CONFIRM = 3
} PackageType_t;

typedef enum PackageValueTag
{
	/*PV_PID, PV_PROCESS, PV_AUTOTRANSMIT, */PV_MESSAGE = 3 /*, PV_SETFLAG, PV_CLEARFLAG, PV_FLAG, PV_SETPOINT, PV_RESETPROCESS, PV_POWER, PV_MEMORY*/
} PackageValue_t;

void testSerial(void)
{
	SerialPort * sp = newgc SerialPort(B9600);
	sp->Open("/dev/ttyUSB0");
	sp->Write("Hello World!\n");

	delete (sp);

}

bool done;

void interpretData(ByteArray & package)
{
	PackageValue_t value = (PackageValue_t) package[1];
	switch (value) {
	case PV_MESSAGE:
		//unsigned char * message = 2 + package.Data();
		ssize_t result = write(1, &package[2], package.size() - 2);
		(void) result;
		//cout << "Message: " << message << endl;
		//printf("Message: %s\n", message);
		break;
	}
}
//char *types[] = {"PT_GET","PT_SET","PT_DATA","PT_CONFIRM"};
void interpretPackage(ByteArray & package)
{
	PackageType_t type = (PackageType_t) package[0];
	//printf("Signal = %s (%i)\n", types[type], type);
	switch (type) {
	case PT_GET:
		break;
	case PT_SET:
		break;
	case PT_DATA:
		interpretData(package);
		break;
	case PT_CONFIRM:
		break;
	}
}

void timerCallback(Timer *sender, ByteArray &data)
{
	(void) sender, (void) data;
	static int ticks = 0;
	stringstream convert;

	//write(1, "Tick...\n", 8);
	//convert << "Tick " << ticks << "...\n";
	//sp->Write(convert.str());
	ticks++;
}

void serialCallback(SerialPort *sender, ByteArray &data)
{
	(void) sender;
	int i;
	unsigned char * package = &data[0];
	//unsigned char * d = data.Data();
	//ByteArray d = *data;
	//write(1, "SP RX: ", 7);
	//write(1, data->Data(), data->ByteSize());
	//printf("Serial RX: ");
	for (i = 0; i < data.size(); i++) {
		//printf("%c", package[i]);
		fputc(package[i], stdout);
	}
	fflush(stdout);
	//printf("\n");
	//ph->ReceivePackage(data);
	//write(1, "\n", 1);
}

void userCallback(IFileDescriptor *sender, ByteArray data)
{
	(void) sender, (void) data;
	if (memcmp(&data[0], "quit", 4) == 0)
		done = true;
//	write(1, "User: ", 6);
//	write(1, data.Data(), data.ByteSize());
//	write(1, "\n", 1);
	//sp->Write(&data);
}

void testRefArray(void)
{
	unsigned int i;
	FixedArray<int*> myArray(3);

	myArray[0] = newgc int(5);
	myArray[1] = newgc int(4);
	myArray[2] = newgc int(3);

	for (i = 0; i < myArray.Count(); i++) {
		cout << i << " " << (*myArray[i]) << endl;
	}
}

void testValueArray(void)
{
	unsigned int i;
	FixedArray<int> myArray(5);
	myArray[0] = 1;
	myArray[1] = 2;
	myArray[2] = 4;
	myArray[3] = 8;
	myArray[4] = 16;
	for (i = 0; i < myArray.Count(); i++) {
		cout << i << " " << myArray[i] << endl;
	}
}

void intHandler(int dummy)
{
	(void) dummy;
	done = true;
}

void testMultiplexer(string serial)
{
	signal(SIGINT, intHandler);

	sp = newgc SerialPort(B500000);
	Multiplexer * mp = newgc Multiplexer();
	IFileDescriptor *user = newgc FileDescriptor(0);
	Timer *timer = newgc Timer();

	sp->Open(serial);

	mp->AddDatasource(*user, (Callback_t) userCallback);
	mp->AddDatasource(*sp, (Callback_t) serialCallback);
	mp->AddDatasource(*timer, (Callback_t) timerCallback);

	timer->SetInterval(2, 0);

	while (!done) {
		mp->Select();
	}
//#ifndef USE_GARBAGE_COLLECTOR
	/*delete (sp);
	 delete (mp);
	 delete (user);
	 delete (timer);
	 delete (ph);*/
//#endif
}

void testPlPlot()
{

}

void * threadCallback(void *str)
{
	int teller = 0;
	while (!done) {
		printf("Hello %i from %s\n", teller++, (char*) str);
		//	sleep(1);
	}
	printf("%s closing ...\n", (char*) str);
	free(str);
	Thread::Exit(NULL);
	return NULL;
}

void testThread(void)
{
	const int N = 100;
	int i;
	char buf[32];
	string x;
	Thread::Init();
	Thread ** threads = newgc Thread*[N];

	for (i = 0; i < N; i++) {
		sprintf(buf, "thread %i", i);
		threads[i] = newgc Thread(threadCallback, strdup(buf));
	}
	//cout << "Press enter to close ..." << endl;
	//cin >> x;

	done = true;
	for (i = 0; i < N; i++) {
		delete (threads[i]);
	}

	delete[] threads;

	Thread::Exit(NULL);
}

void testMatrix(void)
{
	/*	Matrix<double> * A = new(UseGC) Matrix<double>(2, 2);
	 Matrix<double> * B = new(UseGC) Matrix<double>(2, 2);
	 Matrix<double> * C;
	 (*A)(0,0) = 1;
	 (*A)(0,1) = 2;
	 (*A)(1,0) = 3;
	 (*A)(1,1) = 4;

	 (*B)(0,0) = -2;
	 (*B)(0,1) = 1;
	 (*B)(1,0) = 1.5;
	 (*B)(1,1) = -0.5;

	 cout << "A: " << endl << A->ToString();
	 cout << "B: " << endl << B->ToString();

	 C = (*A) * (*B);

	 cout << "C=A*B: " << endl << C->ToString();


	 delete(A);
	 delete(B);
	 delete(C);*/

}

/*int main(int argc, char *argv[])
 {
 testMultiplexer("/dev/ttyUSB0");
 return 0;
 }*/

int main_old(int argc, char *argv[])
{
	(void) argc;
	(void) argv;
	testMatrix();
	return 0;
}

char * filename = (char*) "log.txt";
FILE * gnuplot = NULL;
string current_line = "";
void serialWriteFCallback(SerialPort *sender, ByteArray &data)
{
	(void) sender;
	//unsigned int i;
	//FILE *output = fopen(filename, "ab");
	//fwrite(data.Data(), 1, data.Count(), stdout);

	for (int i = 0; i < data.size(); i++) {
		char c = data[i];
		if (c != '\n') {
			current_line += c;
		} else {
			cout << current_line << endl;
			current_line = "";
		}
	}

	//fflush(stdout);
	//fclose(output);
	//fprintf(gnuplot, "plot \"%s\" using 1:2 with linespoints title \"Sensor 1\"\n", filename);
	//fflush(gnuplot);
}

void userCallback2(IFileDescriptor *sender, ByteArray &data)
{
	(void) sender, (void) data;/*
	 if (memcmp(data.Data(), "quit", 4) == 0) {
	 done = true;
	 }
	 write(1, "User: ", 6);
	 write(1, data.Data(), data.ByteSize());
	 write(1, "\n", 1);*/

}

int main_old3(int argc, char *argv[])
{
	(void) argc;
	(void) argv;
	string s;
	ofstream log;
	string filename = "log.txt";
	//GC_INIT();

	/*if(argc < 2) {
	 printf("Usage: %s <serial_port>\n", argv[0]);
	 return 0;
	 }*/

	gnuplot = popen("gnuplot -persistent", "w");

	SerialPort * sp = newgc SerialPort();

	sp->Open("/dev/ttyUSB1");
	sp->SetBaud(B9600);

	log.open(filename.c_str());

	bool first = true;
	while (!done) {
		s = sp->ReadLine();
		cout << s << endl;
		log << s << endl;
		log.flush();

		if (first) {
			//first = false;
			fprintf(gnuplot,
											"plot \"%s\" using 1:2 with linespoints title \"Sensor 1\",\"%s\" using 1:3 with linespoints title \"Sensor 2\",\"%s\" using 1:4 with linespoints title \"Sensor 3\"\n",
											filename.c_str(), filename.c_str(), filename.c_str());
		} else {
			fprintf(gnuplot, "replot\n");
		}
		fflush(gnuplot);
	}
	sp->Close();
	fclose(gnuplot);
	log.close();
	return 0;
}

int main_old2(int argc, char *argv[])
{
	(void) argc, (void) argv;
	std::vector<IObject*> v;
	if (argc <= 1) {
		printf("Usage: %s <serial_port>\n", argv[0]);
		return 0;
	}
	char * device = argv[1];
	SerialPort * sp = newgc SerialPort();
	sp->Open(device);

	v.push_back(newgc Object());
	v.push_back(newgc Other());
	v.push_back(newgc Timer());
	v.push_back(newgc FileDescriptor(0));
	v.push_back(newgc FixedArray<char*>(10));
	v.push_back(newgc Multiplexer());
	//v.push_back(new PackageHandler(interpretData));
	v.push_back(sp);

	std::vector<IObject*>::iterator it;
	for (it = v.begin(); it != v.end(); it++) {
		cout << (*it)->ToString() << endl;

		//delete(*it);

	}

	//testThread();
	//testPlPlot();
	//testMultiplexer(argv[1]);
	//printf("Hello World");
//	testValueArray();
//	testRefArray();
	return 0;
}

class MySubs1: public Subscriber<int, float>
{
	void Update(int i, float f)
	{
		cout << "MySubs1: " << i << " " << f << endl;
	}
};

class MySubs2: public Subscriber<int, float>
{
	void Update(int i, float f)
	{
		cout << "MySubs2: " << i << " " << f << endl;
	}
};

void publisherTest(void)
{
	Publisher<int, float> pub;
	MySubs1 sub1;
	MySubs2 sub2;

	pub += &sub1;
	pub += &sub2;
	pub.Publish(1, 2.0);
	pub -= &sub1;
	pub.Publish(2, 3.0);
}

void arrayTest(void)
{
	ByteArray myarr;

	myarr = ByteArray(10);

	for (int i = 0; i < myarr.size(); i++) {
		myarr[i] = (uint8_t) i;
		cout << (int) myarr[i] << " ";
	}
	cout << endl;
}

int main(int argc, char *argv[])
{
	(void) argc;
	(void) argv;
	Bitmap bmp( { 256, 256 });
	unsigned int h = bmp.Size().Height;
	unsigned int w = bmp.Size().Width;
	double w2 = w/2.0;
	double h2 = h/2.0;
	for (unsigned int y = 0; y < h; y++) {
		for (unsigned int x = 0; x < w; x++) {
			auto i = 100.0/((x-w2)*(x-w2) + (y-h2)*(y-w2));
			bmp(x,y) = {i,i+0.5,1.0,1.0};
		}
	}
	misc::WritePPM("test.ppm", bmp);

	return 0;
}
