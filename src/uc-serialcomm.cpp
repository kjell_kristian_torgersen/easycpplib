/*
 * uc-serialcomm.cpp
 *
 *  Created on: Sep 11, 2016
 *      Author: kjell
 */

#include <iostream>
#include <iomanip>

#include "SerialPort.hpp"
#include "Multiplexer.hpp"
#include "PackageHandler.hpp"

using namespace std;

bool done = false;

void packageReceivedCallback(ByteArray Package)
{
	uint32_t * ADC_channels = (uint32_t*) &Package[0];

	/*for (unsigned int i = 0; i < Package.size() / sizeof(uint32_t); i++) {
	 cout << fixed << setprecision(7) << 5.0 * ADC_channels[i] / 1024.0 / 1023.0 << " ";
	 }*/
	cout << ADC_channels[0] << " " << ADC_channels[1] << " " <<(double)ADC_channels[1]/16.0 << endl;
	cout.flush();
}

PackageHandler ph(packageReceivedCallback);
void serCallback(SerialPort & sender, ByteArray data)
{
	(void) sender;
	ph.ReceivePackage(data);
}

void keybCallback(IFileDescriptor& sender, ByteArray data)

{
	(void) sender;
	for (int i = 0; i < data.size(); i++) {
		if (data[i] == 'Q')
			done = true;
	}
}

int main(int argc, char *argv[])
{
	SerialPort serialPort;

	if (argc >= 2) {
		serialPort.SetDevice((string) argv[1]);
	} else {
		serialPort.SetDevice("/dev/ttyUSB0");
	}
	serialPort.SetBaud(B9600);

	if (serialPort.Open() == 0) {
		Multiplexer multiPlexer;
		FileDescriptor keyboard(0);

		multiPlexer.AddDatasource(serialPort, (Callback_t) serCallback);
		multiPlexer.AddDatasource(keyboard, keybCallback);
		while (!done) {
			multiPlexer.Select();
		}
	} else {
		cout << "Serial port busy ??" << endl;
	}
	return 0;
}
